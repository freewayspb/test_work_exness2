(function () {
  var startCarousel = function () {
    $('.js-carousel-mobile').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true,
        prevArrow: '<i class="glyphicon glyphicon-chevron-left slider__nav--prev"></i>',
        nextArrow: '<i class="glyphicon glyphicon-chevron-right slider__nav--next"></i>'
      }
    );
  };
  window.onload = function () {
    if(window.innerWidth <= 960){
      startCarousel();
    }
  };
  window.onresize = function() {
    if (window.innerWidth <= 960) {
      startCarousel();
    }
    if (window.innerWidth >= 961) {
      $('.js-carousel-mobile').slick('unslick');
    }
  };
})();
