var gulp        = require('gulp');
var $ = require('gulp-load-plugins')();
var browserSync = require('browser-sync').create();
var reload      = browserSync.reload;
var sourcemaps = require('gulp-sourcemaps');
var pug = require('gulp-pug');
var less = require('gulp-less');
var buildPath = './build';

var del = require('del');
var merge = require('merge-stream');
var runSequence = require('run-sequence');
var argv = require('minimist')(process.argv.slice(2));

// Settings
var RELEASE = !!argv.release;             // Minimize and optimize during a build?
var AUTOPREFIXER_BROWSERS = [
  'ie >= 10',
  'ie_mob >= 10',
  'ff >= 30',
  'chrome >= 34',
  'safari >= 7',
  'opera >= 23',
  'ios >= 7',
  'android >= 4.4',
  'bb >= 10'
];

var src = {};
var watch = false;
var pkgs = require('./package.json').dependencies;

// Run serve as default task
gulp.task('default', ['serve']);

// CSS style sheets
gulp.task('styles', function () {
  src.styles = ['styles/**/*.{css,less}', '!styles/**/_*.{css,less}'];
  return gulp.src(src.styles)
    .pipe($.if(!RELEASE, $.sourcemaps.init()))
    .pipe($.less())
    .pipe($.autoprefixer(AUTOPREFIXER_BROWSERS))
    .pipe($.csscomb())
    .pipe(RELEASE ? $.cssmin() : $.util.noop())
    .pipe($.rename('style.css'))
    .pipe($.if(!RELEASE, $.sourcemaps.write()))
    .pipe(gulp.dest(buildPath + '/css'));
});

// CSS Vendor styles
gulp.task('styles:vendor', function () {
  return merge(
    gulp.src('node_modules/bootstrap/dist/css/bootstrap.min.css')
      .pipe(gulp.dest(buildPath + '/css'))
  );
});

// generates Pages
gulp.task('views', function buildHTML() {
  src.views = ['layouts/**/*.pug', '!layouts/**/_*.pug'];
  src.watch = 'layouts/**/*.pug';
  return gulp.src(src.views)
    .pipe(pug({
      // Your options in here.
    }))
    .pipe(gulp.dest(buildPath));
});

// Static files
gulp.task('assets', function () {
  src.assets = 'assets/**';
  return merge(
    gulp.src(src.assets)
    .pipe(gulp.dest(buildPath + '')),
    gulp.src('node_modules/owl.carousel/dist/assets/*.{css,gif}')
      .pipe(gulp.dest(buildPath + '/assets'))
  );
});

// Images
gulp.task('images', function () {
  src.images = 'img/**';
  return gulp.src(src.images)
    .pipe($.cache($.imagemin({
      progressive: true,
      interlaced: true
    })))
    .pipe(gulp.dest(buildPath + '/img'));
});

// Copy fonts
gulp.task('fonts', function () {
  return gulp.src(['node_modules/bootstrap/fonts/**', 'fonts/**'])
      .pipe(gulp.dest(buildPath + '/fonts'));
});

// 3rd party libraries
gulp.task('vendor', function () {
  return merge(
    gulp.src('node_modules/jquery/dist/*.*')
      .pipe(gulp.dest(buildPath + '/vendor/jquery-' + pkgs.jquery)),
    gulp.src('node_modules/modernizr/dist/modernizr-build.min.js')
      .pipe($.rename('modernizr.min.js'))
      .pipe($.uglify())
      .pipe(gulp.dest(buildPath + '/vendor/modernizr-' + pkgs.modernizr)),
    gulp.src('node_modules/bootstrap/dist/js/bootstrap.min.js')
      .pipe(gulp.dest(buildPath + '/vendor/bootstrap-' + pkgs.bootstrap)),
    gulp.src('node_modules/owl.carousel/dist/owl.carousel.min.js')
      .pipe(gulp.dest(buildPath + '/vendor/owl-' + pkgs.owl))
  );
});

// JavaScript
gulp.task('scripts', function () {
  src.scripts = ['scripts/plugins.js', 'scripts/main.js'];
  return gulp.src(src.scripts)
    .pipe($.if(!RELEASE, $.sourcemaps.init()))
    .pipe($.concat('bundle.js'))
    .pipe($.if(RELEASE, $.uglify()))
    .pipe($.if(!RELEASE, $.sourcemaps.write()))
    .pipe(gulp.dest(buildPath + '/js'));
});

// Clean up
gulp.task('clean', del.bind(null, ['build/*', '!build/.git'], {dot: true}));// Clean up
gulp.task('clean', del.bind(null, ['build/*', '!build/.git'], {dot: true}));

// Run BrowserSync
gulp.task('serve', ['build'], function () {

  var path = require('path');
  var url = require('url');
  var fs = require('fs');
  var browserSync = require('browser-sync');

  browserSync({
    notify: false,
    // Run as an https by uncommenting 'https: true'
    // Note: this uses an unsigned certificate which on first access
    //     will present a certificate warning in the browser.
    // https: true,
    server: {
      baseDir: buildPath,
      middleware: function (req, res, cb) {
        var uri = url.parse(req.url);
        if (uri.pathname.length > 1 &&
          path.extname(uri.pathname) === '' &&
          fs.existsSync(buildPath + uri.pathname + '.html')) {
          req.url = uri.pathname + '.html' + (uri.search || '');
        }
        cb();
      }
    }
  });

  gulp.watch(src.assets, ['assets']);
  gulp.watch(src.images, ['images']);
  gulp.watch(src.watch, ['views']);
  gulp.watch(src.styles, ['styles']);
  gulp.watch(src.scripts, ['scripts']);
  gulp.watch(buildPath + '/**/*.*', function(file) {
    browserSync.reload(path.relative(__dirname, file.path));
  });
  watch = true;
});

// Build
gulp.task('build', ['clean'], function (cb) {
  runSequence(['vendor', 'styles:vendor', 'assets', 'images', 'fonts', 'views', 'styles', 'scripts'], cb);
});