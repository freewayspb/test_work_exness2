Test task 2
=========

### Conditions

- Browsers (Chrome, Firefox, Safari, IE10+).
- Adaptive layout for: Android, iOS, Windows Mobile.
- Min resolution: 480px
- Max resolution: 1280px

### Setup

These instructions will help you set up this project locally in order to modify it directly.

#### Installation

1. Install the following utilities:
    - [Git](http://git-scm.com/downloads): a free and open source distributed version control system.
        - Unfamiliar with Git? I recommend using [GitHub for Windows](https://windows.github.com/) or [Github for Mac](https://mac.github.com/).
    - [Node.js](http://nodejs.org/download/): a software platform with npm packages manager.
    - [Gulp](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md): a JavaScript task runner.

            $ sudo npm install -g gulp-cli
2. Clone this repository to wherever you prefer to keep projects:

        $ git clone https://bitbucket.org/freewayspb/test_work_exness2

3. Navigate to where you've installed folder:

        $ cd ~/.../installed-folder

4. Install the dependencies:

        $ npm install
        
#### Running the local server

- Run the app from your localhost:

        $ gulp

- The development harness should automatically open in your default browser. If not, visit [http://localhost:3000/](http://localhost:3000/) in your preferred browser.
Note that you can optionally serve the app on another port: `$ gulp serve --port=xxxx`